# Brackets Problem

Write a method, say, 

```
boolean IsWellFormedBrackets(String inputString)
```

which will take a string input and check whether the input string is a valid well formed bracket string with proper opening and closing bracket.

Extend your solution to cater to all kinds of brackets viz. [], {} and () 

Extend your solution to take a file input say "HelloWorld.java" and then check whether the file is a syntactically valid java program file.

